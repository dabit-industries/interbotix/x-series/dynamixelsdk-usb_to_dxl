#define DXL_USB_VER           20190714

#define CMD_PORT              Serial      // USB
#define DBG_PORT              Serial2     // UART1

#if defined(__OPENCM904__)
// TODO: Use DynamixelSDK PortHandler?
#define DXL_CHANNEL           0 // Channel from PortHandler
#define DXL_PORT              Serial1 // Serial3
#else
#define DXL_PORT              Serial3 // Serial3
#endif

#define DXL_BAUD              1000000

#if defined(__OPENCM904__)
#define DXL_LED_RX            BOARD_LED_PIN // BDPIN_LED_USER_1
#define DXL_LED_TX            BOARD_LED_PIN // BDPIN_LED_USER_2
#else
#define DXL_LED_RX            BDPIN_LED_USER_1
#define DXL_LED_TX            BDPIN_LED_USER_2
#endif

#define DXL_POWER_DISABLE()   digitalWrite(BDPIN_DXL_PWR_EN, LOW);
#define DXL_POWER_ENABLE()    digitalWrite(BDPIN_DXL_PWR_EN, HIGH);

volatile uint8_t rx_led_count = 0;
volatile uint8_t tx_led_count = 0;

HardwareTimer Timer(TIMER_CH1);

void setup()
{
  CMD_PORT.begin(1000000);
  //DBG_PORT.begin(57600);

#if defined(__OPENCM904__)
  drv_uart_set_dxl_mode(DXL_CHANNEL, true);
#endif
  DXL_PORT.begin(DXL_BAUD);

#if not defined(__OPENCM904__)
  pinMode( BDPIN_DXL_PWR_EN, OUTPUT );
#endif
  pinMode( DXL_LED_RX, OUTPUT );
  pinMode( DXL_LED_TX, OUTPUT );

  digitalWrite(DXL_LED_TX, HIGH);
  digitalWrite(DXL_LED_RX, HIGH);

#if defined(__OPENCM904__)
  drv_dxl_begin(DXL_CHANNEL);
  drv_dxl_tx_enable(DXL_CHANNEL, FALSE);
#else
  drv_dxl_tx_enable(FALSE);
  DXL_POWER_ENABLE();
#endif

  // LED Timer
  Timer.stop();
  Timer.setPeriod(50000); // 50ms seems okay for an update rate
  Timer.attachInterrupt(update_led);
  Timer.start();
}

void dxl_direction(bool dir)
{
  // dir = true for dynamixel rx
  // dir = false for dynamixel tx
#if defined(__OPENCM904__)
  drv_dxl_tx_enable(DXL_CHANNEL, dir);
#else
  drv_dxl_tx_enable(dir);
#endif
}

void loop()
{
  static uint16_t length;
  static uint16_t i;

  //-- USB -> DXL
  length = CMD_PORT.available();
  if( length > 0 )
  {
    dxl_direction(true);
    for(i=0; i<length; i++ )
    {
      DXL_PORT.write(CMD_PORT.read());
    }
    // Make sure data is written before switching direction
    DXL_PORT.flush();
    
    tx_led_count = 3;
  }

  //-- DXL -> USB
  dxl_direction(false);
  length = DXL_PORT.available();
  if( length > 0 )
  {
    if (length > SERIAL_BUFFER_SIZE)
    {
      length = SERIAL_BUFFER_SIZE;
    }
    for(i=0; i<length; i++ )
    {
      CMD_PORT.write(DXL_PORT.read());
    }
 
    rx_led_count = 3;
  }
}


void update_led()
{
  static bool tx_led_state = LOW;
  static bool rx_led_state = LOW;
  
#if not defined(__OPENCM904__)
  if( tx_led_count )
  {
    tx_led_state = !tx_led_state;
    digitalWrite(DXL_LED_TX, tx_led_state);
    tx_led_count--;
  }
  else
  {
    digitalWrite(DXL_LED_TX, HIGH);
  }
#endif
  // We really care about seeing dynamixel packets if we only have 1 led
  if( rx_led_count )
  {
    rx_led_state = !rx_led_state;
    digitalWrite(DXL_LED_RX, rx_led_state);
    rx_led_count--;
  }
  else
  {
    digitalWrite(DXL_LED_RX, HIGH);
  }
}
