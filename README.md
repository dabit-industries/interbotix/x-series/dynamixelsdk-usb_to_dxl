# USB to DXL
Firmware to forward serial from USB to DXL.

-----

# About
This repo contains firmware for enabling USB to Dynamixel passthrough for the OpenCM9.04c.  
It is also compatible with OpenCR boards.  

# Usage
1. Follow the [Arduino IDE Setup](http://emanual.robotis.com/docs/en/software/arduino_ide/) from Robotis  
    1. Make sure you set up your UDEV rules properly!  
2. Load the `usb_to_dxl` sketch into Arduino  
3. Upload  
    1. Make sure you select OpenCM or OpenCR from your board manager!  

# TODO
- Use DynamixelSDK for finding ports  
  - Enables support for OpenCM variants other than the OpenCM9.04c  
